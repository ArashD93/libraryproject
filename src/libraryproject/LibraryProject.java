package libraryproject;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class LibraryProject {
    
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    static Connection connection;
    static Scanner sc = new Scanner (System.in);
    
    private void connect(){
        try{
            MysqlDataSource ds= new MysqlDataSource();
            ds.setPort(3306);
            ds.setServerName("Localhost");
            ds.setUser("root");
            Scanner sc1=new Scanner(new File("src/libraryproject/SQLDatabase.txt"));
            String password=sc1.nextLine().trim();
            ds.setPassword(password);
            ds.setDatabaseName("mydb");
            connection=ds.getConnection();
        }catch(FileNotFoundException e){
            System.out.println("A fájl nem található!");
        }catch(SQLException e){
            System.out.println("Nem sikerült kapcsolódni az adatbázishoz!");
        }
    }
    
    public static Connection getConnection(){
        return connection;
    }
    
    public static void main(String[] args){
        MainMenu mm = new MainMenu();
        LibraryProject lp= new LibraryProject();
        lp.connect();
        
        
        int back;
        do{
            mm.chooseWhatToDo();
            System.out.println(ANSI_GREEN+"0: Visszalépés a menübe");
            System.out.println(ANSI_RED+"1: Kilépés");
            back=sc.nextInt();
        }while( back==0);
        mm.endProject();
        
    }  
    
    
   
}
