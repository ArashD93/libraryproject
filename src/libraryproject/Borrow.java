
package libraryproject;

//előfoglalások kezelése

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static libraryproject.LibraryProject.connection;

public class Borrow {
    
    int user_userId;
    String author;
    String title;

    public Borrow(int user_userId, String author, String title) {
        this.user_userId = user_userId;
        this.author = author;
        this.title = title;
    }
    
    public Borrow(){
        
    }
    
    public void preReservation() throws SQLException{
        //try {
            Connection connection=LibraryProject.getConnection();
            PreparedStatement ps1=connection.prepareStatement("insert into reserve (user_userid, author, title) values (?,?,?)");
            ps1.setInt(1, user_userId);
            ps1.setString(2, author);
            ps1.setString(3, title);
            ps1.execute();
            System.out.println("Az előfoglalás a(z): " + author + title + " könyvre megtörtént");
            List<Integer> borrowList = new ArrayList<>();
            PreparedStatement ps=connection.prepareStatement(
                    "SELECT BookId FROM mydb.book where (author=?) and (title=?)");
            ps.setString(1, author);
            ps.setString(2, title);
            ResultSet rs= ps.executeQuery();
            while(rs.next()){
                int bookIds=rs.getInt("bookId");
                borrowList.add(bookIds);
            }
            if (borrowList.isEmpty()) {
                System.out.println("A");
            }
        //} catch (SQLException ex) {
            //System.out.println("Az adatbázissal kapcsolatos hiba történt!");
        //}
    }
}
