
package libraryproject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Statistics {
    
    //legnépszerűbb könyvek
    public void mostPopularBook() {
        try{
            Connection connection=LibraryProject.getConnection();
            PreparedStatement ps= connection.prepareStatement(
                "SELECT author,title FROM book join borrow "
                        + "GROUP BY borrow.Book_bookId "
                        + "ORDER BY COUNT(User_UserId) DESC LIMIT 1");
            ResultSet rs= ps.executeQuery();
            //while(rs.next()){
                String bookAuthor=rs.getString("author");
                String bookTitle=rs.getString("title");
                System.out.println(bookAuthor+": "+bookTitle+" a legnépszerűbb könyv.");
            //}
        }catch(SQLException e){
            System.out.println("Az adatbázissal kapcsolatos hiba történt!");
        }
    }
    
    //az egyes könyvekre a kintlét átlagos időtartama
    //TODO...
    public void avgBorrowTime(int borrowId) throws SQLException{
        try{
            Connection connection=LibraryProject.getConnection();
            PreparedStatement ps1=connection.prepareStatement("SELECT DATEDIFF "
                    + "((select returndate from mydb.borrow where borrowId=?), \n" +
                      "(select borrowdate from mydb.borrow where borrowId=?)) AS DiffDate");
            ps1.setInt(1, borrowId);
            ps1.setInt(2, borrowId);
            ResultSet rs4=ps1.executeQuery();
            int borrowTime;
            while(rs4.next()){
                borrowTime=rs4.getInt("datediff");
                System.out.println();
            }
        }catch(SQLException e){
            System.out.println("Az adatbázissal kapcsolatos hiba történt!");
        }
    }
    
    //melyik felhasználó hány könyvet kölcsönzött már ki
    public void howManyBooksBorrowed(int userId){
        try{
            Connection connection=LibraryProject.getConnection();
            PreparedStatement ps2 = connection.prepareStatement(
                    "select distinct sum(Book_BookId) from mydb.borrow where User_UserId = ?");
            ps2.setInt(1, userId);
            ResultSet rs= ps2.executeQuery();
            int sumBooked=0;
            while(rs.next()){
                sumBooked = rs.getInt("sum(Book_BookId)");
            }
            //ide a szép kiíratást írjuk meg!
            System.out.println(sumBooked);
        }catch(SQLException e){
            System.out.println("Az adatbázissal kapcsolatos hiba történt!");
        }
    }
}
