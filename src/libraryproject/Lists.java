
package libraryproject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static libraryproject.LibraryProject.connection;


public class Lists {
      
    //felhasználók és a náluk lévő könyvek listája
    public void usersHasBooks(){
        try{
            List<String> usersAndBooksList = new ArrayList<>();
            PreparedStatement ps=connection.prepareStatement(
                    "select Email, Author, Title from mydb.user join mydb.borrow join mydb.book");
            ResultSet rs= ps.executeQuery();
            String userEmail;
            String bookAuthor;
            String bookTitle;
            while(rs.next()){
                userEmail = rs.getString("Email");
                usersAndBooksList.add(userEmail);
                bookAuthor = rs.getString("Author");
                usersAndBooksList.add(bookAuthor);
                bookTitle = rs.getString("Title");
                usersAndBooksList.add(bookTitle);
            }
            for (int i = 0; i < usersAndBooksList.size(); i++) {
                System.out.print("");
            }
        }catch(SQLException e){
            System.out.println("Az adatbázissal kapcsolatos hiba történt!");
        }
    }
    
    //egy felhasználónál lévő könyvek listája
    public void givenUserHasBooks(int userId){
        try{
            List<String> bookIdList = new ArrayList<>();
            PreparedStatement ps = connection.prepareStatement(
                    "select Email, Author, Title from mydb.user join mydb.borrow join mydb.book where UserId = ?");
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                String userEmail = rs.getString("Email");
                bookIdList.add(userEmail);
                String bookAuthor = rs.getString("Author");
                bookIdList.add(bookAuthor);
                String bookTitle = rs.getString("Title");
                bookIdList.add(bookTitle);
            }
            //ide a szép kiíratást írjuk meg!
            System.out.println(bookIdList);
        }catch(SQLException e){
            System.out.println("Az adatbázissal kapcsolatos hiba történt!");
        }
    }
    
    //egy könyv összes korábbi kölcsönzésének száma
    public void totalBorrow(String title){
        try{
            PreparedStatement ps = connection.prepareStatement("select count(BorrowDate) from"
                    +" mydb.borrow join mydb.Book on borrow.Book_BookId=book.BookId where Title = ?");
            ps.setString(1, title);
            ResultSet rs= ps.executeQuery();
            int sumBorrowed = 0;
            while(rs.next()){
                sumBorrowed= rs.getInt("count(BorrowDate)");
            }
            System.out.println("A könyvet eddig " + sumBorrowed + " alkalommal kölcsönözték ki.");
        }catch(SQLException e){
            System.out.println("Az adatbázissal kapcsolatos hiba történt!");
        }
    }
    
    //egy adott napon történő teljes forgalom listája (kölcsönzések és visszahozott könyvek)
    public void totalBorrowAndBack(String requiredDay ){
        try{
            List<String> requiredDayList= new ArrayList<>();
            PreparedStatement ps = connection.prepareCall("select title from mydb.borrow "
                    + "join mydb.Book on borrow.Book_BookId=book.BookId where (borrowDate=?) or (returnDate=?)");
            ps.setString(1, requiredDay);
            ps.setString(2, requiredDay);
            ResultSet rs=ps.executeQuery();
            while(rs.next()){
                String titles;
                titles=rs.getString("title");
                requiredDayList.add(titles);
                //ide a szép kiíratást írjuk meg!
                System.out.println(requiredDayList);
            }
        }catch(SQLException e){
            System.out.println("Az adatbázissal kapcsolatos hiba történt!");
        }
    }
}
