
package libraryproject;

import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.*;


public class MainMenu {
    
    Lists l = new Lists();
    Statistics s = new Statistics();
    
    public void chooseWhatToDo(){

        try {
            menuText();
            
            Scanner sc=new Scanner(System.in);
            int whatToDo = 0;
            whatToDo = sc.nextInt();
            sc.nextLine();
            Pattern emailRegExp=Pattern.compile("^[A-Za-z0-9+_.-]+@(.+)$");
            Pattern isbnRepExp=Pattern.compile("^\\d{10}$");
            
            switch (whatToDo){
                case 1:
                    System.out.println("Adja meg a felhasználó email címét!");
                    String email;
                    email=sc.nextLine().trim();
                    Matcher matcher = emailRegExp.matcher(email);
                    while(!matcher.find()){
                        System.out.println( "Nem megfelelő email cím");
                        System.out.println("Adjon meg egy érvényes email címet");
                        email=sc.nextLine().trim();
                        matcher = emailRegExp.matcher(email);
                    }
                    System.out.println("Az email cím megfelel");
                    User u1=new User(email);
                    u1.addNewUser();
                    break;
                case 2:
                    System.out.println(
                            "Ha könyvet szeretne kölcsönözni, nyomja meg az 1-est \n Ha előfoglalást szeretne leadni egy könyvre, nyomja meg a 2-est!");
                    int borrowOrReserve=sc.nextInt();
                    sc.nextLine();
                        switch(borrowOrReserve){
                            case 1:
                                System.out.println("Adja meg a felhasználó azonosítóját(id)!");
                                int userId;
                                userId=sc.nextInt();
                                sc.nextLine();
                                User u2=new User(userId);
                                System.out.println("Adja meg a könyv szerzőjét!");
                                String author;
                                author=sc.nextLine().trim();
                                System.out.println("Adja meg a könyv címét!");
                                String title;
                                title=sc.nextLine().trim();
                                u2.borrowABook(author, title);
                                break;
                            case 2:
                                System.out.println("Adja meg a felhasználó azonosítóját(id)!");
                                userId=sc.nextInt();
                                sc.nextLine();
                                System.out.println("Adja meg a könyv szerzőjét!");
                                author=sc.nextLine().trim();
                                System.out.println("Adja meg a könyv címét!");
                                title=sc.nextLine().trim();
                                Borrow borrow= new Borrow();
                                borrow.preReservation();
                                break;
                            default:
                                System.out.println("Hibás menü válsztás");
                        }
                case 3:
                    System.out.println("Adja meg a visszaadni kívánt könyv azonosítóját!");
                    int bookBackId;
                    bookBackId=sc.nextInt();
                    sc.nextLine();
                    User u3= new User();
                    u3.returnABook(bookBackId);
                    break;
                case 4:
                    System.out.println("Adja meg a könyv címét!");
                    Book b1=new Book();
                    b1.title=sc.nextLine().trim();
                    System.out.println("Adja meg a könyv szerzőjét!");
                    b1.author=sc.nextLine().trim();
                    System.out.println("Adja meg a könyv oldalszámát!");
                    b1.pngNumber=sc.nextLine().trim();
                    System.out.println("Adja meg a könyv ISBN kódját(10 darab szám karakterből kell állnia)!");
                    b1.ISBN=sc.nextLine().trim();
                    matcher= isbnRepExp.matcher(b1.ISBN);
                    while(!matcher.find()){
                        System.out.println( "Nem megfelelő ISBN szám");
                        System.out.println("Adjon meg egy érvényes ISBN számot");
                        b1.ISBN=sc.nextLine().trim();
                        matcher = isbnRepExp.matcher(b1.ISBN);
                    }
                    b1.addNewBook();
                    break;
                case 5:
                    System.out.println("Adja meg a könyv azonosítóját(id)!");
                    int bookId=sc.nextInt();
                    sc.nextLine();
                    Book b2=new Book(bookId);
                    b2.deleteBook();
                    break;
                case 6:
                    listMenuText();
                    int lists =0;
                    lists=sc.nextInt();
                    sc.nextLine();
                    switch(lists){
                        case 1:
                            l.usersHasBooks();
                            break;
                        case 2:
                            System.out.println("Adja meg a felhasználó azonosítóját(id)!");
                            int userId2=sc.nextInt();
                            sc.nextLine();
                            l.givenUserHasBooks(userId2);
                            break;
                        case 3:
                            //ezen még dolgozzunk, mert cím alapján több könyvet is kidobhat
                            //meg azt is kezelni kéne, hogy egy könyvből van több példány és
                            //azok összesen adják majd az összkölcsönzést
                            System.out.println("Adja meg a könyv címét!");
                            String title=sc.nextLine();
                            l.totalBorrow(title);
                            break;
                        case 4:
                            System.out.println("Adja meg a dátumot!");
                            //DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                            //while( requiredDay==null){
                            String requiredDay = sc.nextLine();
                            //requiredDay= format.parse(line);
                            //}
                            l.totalBorrowAndBack(requiredDay);
                            break;
                        default:
                            System.out.println("Hibás menü válsztás");
                    }
                    break;
                case 7:
                    statMenuText();
                    int stats =0;
                    stats=sc.nextInt();
                    sc.nextLine();
                    switch(stats){
                        case 1:
                            Book b=new Book();
                            s.mostPopularBook();
                            break;
                        case 2:
                            System.out.println("Adja meg a könyv címét!");
                            int borrowId=sc.nextInt();
                            sc.nextLine();
                            b=new Book();
                            s.avgBorrowTime(borrowId);
                            break;
                        case 3:
                            System.out.println("Kérem a felhasználói azonosítóját!");
                            User u= new User();
                            u=new User();
                            int userId=sc.nextInt();
                            s.howManyBooksBorrowed(userId);
                            break;
                        default:
                            System.out.println("Hibás menü válsztás");
                    }
                    break;
                case 8:
                    LibraryProject lp = new LibraryProject();
                    endProject();
                    break;
                default:
                    System.out.println("Hibás menü válsztás");
            }
        } catch (SQLException ex) {
            Logger.getLogger(LibraryProject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void endProject(){
        System.out.println("Sikeres kilépés");
    }
    
    private void menuText(){
        System.out.println("Válasszon az alábbi menüből:");
        System.out.println("1: Új felhasználó létrehozása");
        System.out.println("2: Kölcsönzés vagy előfoglalás");
        System.out.println("3: Könyv vissza");
        System.out.println("4: Könyv hozzáadása");
        System.out.println("5: Könyv törlése");
        System.out.println("6: Listák ");
        System.out.println("7: Statisztikák");
        System.out.println("8: Kilépés");
    }
    
    private void listMenuText(){
        System.out.println("Adja meg a kívánt lista sorszámát!");
        System.out.println("1: Felhasználók és a náluk lévő könyvek listája");
        System.out.println("2: Adott felhasználónál lévő könyvek listája");
        System.out.println("3: Egy adott könyv korábbi kölcsönzéseinek listája");
        System.out.println("4: Adott napon történő teljes forgalom (kölcsönzések és visszahozott könyvek)");
    }
    
    private void statMenuText(){
        System.out.println("Adja meg a kívánt statisztika sorszámát!");
        System.out.println("1: Legnépszerűbb könyvek");
        System.out.println("2: Átlagos kölcsönzési-idő adott könyvre");
        System.out.println("3: Adott felhasználó eddig hány könyvet kölcsönzött ki");
    }
    
}
