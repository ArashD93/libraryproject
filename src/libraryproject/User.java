package libraryproject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User {
    
    PreparedStatement ps;
    
    int userId;
    Date regDate;
    String email;
    
    public User(){
        
    }

    public User(String email) {
        this.email = email;
    }
    
    public User(int userId) {
        this.userId=userId;
    }
    
    //új felhasználó hozzáadása
    public void addNewUser(){
        try{
            Connection connection=LibraryProject.getConnection();
            List<String> emailList = new ArrayList<>();
            PreparedStatement ps=connection.prepareStatement("select email from user where email=?");
            ps.setString(1, email);
            ResultSet rs= ps.executeQuery();
            while(rs.next()){
                String validEmail=rs.getString("email");
                emailList.add(validEmail);
            }
            if(emailList.isEmpty()){
                ps=connection.prepareStatement("insert into user (RegistrationDate, email,PaymentDate) values (?,?,?)");
                ps.setTimestamp(1, new java.sql.Timestamp(new java.util.Date().getTime()));
                ps.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
                ps.setString(2, email);
                ps.execute();
                System.out.println(email+" felhasználó hozzáadása az adatbázishoz megtörtént.");
            }else{
                System.out.println("Ez az email cím szerepel az adatbázisban");
            }
        }catch(SQLException e){
            System.out.println("Az adatbázissal kapcsolatos hiba történt!");
        }
    }
    
    public void borrowABook(String author, String title){
        try{
            Connection connection=LibraryProject.getConnection();
            System.out.println(author+": " + title+" keresése...");
            List<Integer> idList = new ArrayList<>();
            ps=connection.prepareStatement(
                    "SELECT BookId FROM mydb.book where (author=?) and (title=?)");
            ps.setString(1, author);
            ps.setString(2, title);
            ResultSet rs= ps.executeQuery();
            while(rs.next()){
                int nextBookId=rs.getInt("idBooks");
                idList.add(nextBookId);
            }
            if (idList.isEmpty()) {
                System.out.println("A könyv nincs meg a könyvtárban!");
            }else{
                Integer bookId =0;
                for (Integer integer : idList) {
                    List<Integer> idList2 = new ArrayList<>();
                    ps=connection.prepareStatement(
                            "SELECT Book_BookId FROM mydb.borrow where (Book_BookId=?) and (isBorrowed=true)");
                    ps.setInt(1, integer);
                    ResultSet rs1= ps.executeQuery();
                    while(rs1.next()){
                        int nextBookId=rs1.getInt("Book_BookId");
                        idList2.add(nextBookId);
                    }
                    if (idList2.isEmpty()) {
                        bookId=integer;
                        System.out.println("A könyv kölcsönözhető.");
                        break;
                    }
                }
                if (bookId==0) {
                    System.out.println("A könyv nem kölcsönözhető!");
                }else{
                    List<Integer> idlist3= new ArrayList<>();
                    ps=connection.prepareStatement("SELECT userId FROM mydb.user where UserId= ?");
                    ps.setInt(1, userId);
                    ResultSet rs2=ps.executeQuery();
                    while(rs2.next()){
                        int validUserId=rs2.getInt("userId");
                        idlist3.add(validUserId);
                    }
                    if(idlist3.isEmpty()){
                        System.out.println("invalid id, írjá' be újat");
                    }else{    
                        ps=connection.prepareStatement("insert into mydb.borrow (Book_BookId, User_UserId, BorrowDate) values (?,?,?)");
                        ps.setInt(1, bookId);
                        ps.setInt(2, userId);
                        ps.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
                        ps.execute();
                        System.out.println(author+": "+title+" könyv kölcsönzése megtörtént.");
                    }
                }
            }
        }catch(SQLException e){
            System.out.println("Az adatbázissal kapcsolatos hiba történt!");
        }
    }
    
    //a felhasználó visszahozza a könyvet
    public void returnABook(int bookId){
        try{
            Connection connection=LibraryProject.getConnection();
            List<Integer> borrowedList = new ArrayList<>();
            ps=connection.prepareStatement(
                    "SELECT Book_BookId FROM mydb.borrow where (Books_idBooks=?)");
            ps.setInt(1, bookId);
            ResultSet rs= ps.executeQuery();
            while(rs.next()){
                int nextBookId=rs.getInt("Book_BookId");
                borrowedList.add(nextBookId);
            }
            if (borrowedList.isEmpty()) {
                System.out.println("A könyv nincs kikölcsönözve");
            }else{
                for (Integer integer : borrowedList) {
                    ps=connection.prepareStatement(
                            "update mydb.borrow set ReturnDate = ?,where Book_BookId= ?");
                    ps.setTimestamp(1, new java.sql.Timestamp(new java.util.Date().getTime()));
                    ps.setInt(2, bookId);
                    ps.execute();
                } 
            }
        }catch(SQLException e){
            System.out.println("Az adatbázissal kapcsolatos hiba történt!");
        }
    }

    
}
