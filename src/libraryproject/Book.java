package libraryproject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Book {
    
    int bookId;
    String title;
    String author;
    String pngNumber;
    String ISBN;
    
    public Book(String author, String title, String pngNumber, String ISBN) {
        this.author = author;
        this.title = title;
        this.pngNumber = pngNumber;
        this.ISBN = ISBN;
    }
    
    public Book(){
        
    }
    
    public Book(int bookId){
        this.bookId=bookId;
    }

    //könyv hozzáadása, új könyv felvétele az adatbázisba
    public void addNewBook(){
        try{
            Connection connection=LibraryProject.getConnection();
            PreparedStatement ps=connection.prepareStatement("insert into book (author,title,PageNr,ISBN) values (?,?,?,?)");
            ps.setString(1,author);
            ps.setString(2, title);
            ps.setString(3, pngNumber);
            ps.setString(4, ISBN);
            ps.execute();
            System.out.println(author+": "+title+" cimű könyv felvétele a könyvtárba megtörtént.");
        }catch(SQLException e){
            System.out.println("Az adatbázissal kapcsolatos hiba történt!");
        }
    }
    
    //könyv leselejtezése(törlése)
    public void deleteBook(){
        try{
            Connection connection=LibraryProject.getConnection();
            PreparedStatement delps=connection.prepareStatement("update book set isDeleted=true where BookId= ?");
            delps.setInt(1, bookId);
            delps.execute();
            System.out.println(bookId+" azonosítójú könyv selejtezése megtörtént.");
        }catch(SQLException e){
            System.out.println("Az adatbázissal kapcsolatos hiba történt!");
        }
    }
    
 
     
}